<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arMonths_r = array();
for ($i = 1; $i <= 12; $i++)
{
    $arMonths_r[$i] = ToLower(GetMessage('MONTH_'.$i.'_S'));
}


echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">';
echo '<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>';
echo '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>';

?>

<script>

    $(function () {
        $('.send-comment-button').click(function (e) {
            var id = e.target.id.replace('button_', '');

            $.ajax({
                type: 'post',
                url: '<?= $templateFolder ?>' + '/comment.php',
                data: {
                    id: id,
                    comment: $('#text_' + id).val()
                }
            }).done(function(){
                location.reload();
            });
        });
    });

</script>

<div class="bx-birthday-layout-include">
    <?if($arResult["CURRENT"]):?>
        <?foreach ($arResult['CURRENT'] as $current): ?>
            <div class="birthday_now">
                <div class="inner-birthday">
                    <h2>������� ���������</h2>

                    <div class="crown_container" style="width: 1px !important; height: 1px !important;">
                        <img class="crown" src="/local/components/mcart/intranet.structure.birthday.nearest/templates/include_area/images/crown.png" width="100px" height="100px"/>
                    </div>

                    <?if($current['BIG_PHOTO']):?>
                        <img src='<?=$current['BIG_PHOTO']?>' width = "200px" height = "250px"/><br>
                    <?else:?>
                        <img src="/local/components/mcart/intranet.structure.birthday.nearest/templates/include_area/images/no_photo.jpg" width = "200px" height = "250px"/><br>
                    <?endif;?>
                    <div class="name">
                        <h2>
                            <?= $current['NAME']?><br>
                        </h2>
                    </div>
                    <div class="congratulation">
                        <button type="button" data-toggle="modal" data-target="#myModal_<?=$current['ID']?>">�������� ������������</button><br><br>
                    </div>

                    <div class="modal fade" id="myModal_<?=$current['ID']?>" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">��� �����������:</h4>
                                </div>
                                <div class="modal-body">
                                    <textarea id="text_<?=$current['ID']?>" rows="10" cols="32"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button id='button_<?=$current['ID']?>' type="button" class="btn btn-default send-comment-button" data-dismiss="modal">��������</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?if($arResult["COMMENTS_BY_ID"][$current["ID"]]):?>
                        <div class="text_comments">
                            <p class="cong_text">
                                <?echo iconv("UTF-8", "Windows-1251", $arResult["COMMENTS_BY_ID"][$current["ID"]]);?>
                            </p>
                        </div>
                    <?endif;?>

                    <img src="<?=$templateFolder.'/images/pen.PNG';?>"/>
                    <a id="open_messeger_<?=$current["ID"];?>" class="open-messeger">�������� ���������</a>
                    <script>
                        $('#open_messeger_<?=$current["ID"];?>').on('click',function(){
                        BXIM.openMessenger(<?=$current["ID"];?>)
                        });
                    </script>
                </div>
            </div><br>
        <? endforeach; ?>
    <?else:?>
        <p>������� ����������� ���</p>
    <?endif;?>
</div>

<?
global $USER;
$us = $USER->GetID();
$arGroups = CUser::GetUserGroup($us);
?>

<? if(in_array("1", $arGroups)):?>
    <script type="text/javascript">
        $('.congratulation').css("display","block");
    </script>
<?endif; ?>

