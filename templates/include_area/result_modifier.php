<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

const MY_HL_BLOCK_ID = 1;

CModule::IncludeModule('highloadblock');

if(!function_exists('GetEntityDataClass')) {
    function GetEntityDataClass($HlBlockId)
    {
        if (empty($HlBlockId) || $HlBlockId < 1) {
            return false;
        }
        $hlblock = HLBT::getById($HlBlockId)->fetch();
        $entity = HLBT::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        return $entity_data_class;
    }
}

$entity_data_class = GetEntityDataClass(MY_HL_BLOCK_ID);
$rsData = $entity_data_class::getList(array(
    'select' => array('*')
));

$rsData = $entity_data_class::getList(array(
    'select' => array('*')
));
$elements = array();
while($el = $rsData->fetch()){
    $elements[]=$el['UF_ID'];
}
$arResult["ELEMENTS_ID"] = $elements;

$arParams['NAME_TEMPLATE'] = $arParams['NAME_TEMPLATE'] ? $arParams['NAME_TEMPLATE'] : CSite::GetNameFormat(false);

if ($arParams['bShowFilter'])
{
	$dbCurrentUser = CUser::GetByID($GLOBALS['USER']->GetID());
	$arResult['CURRENT_USER'] = $dbCurrentUser->Fetch();
	if ($arParams['bShowFilter'] = !!($arResult['CURRENT_USER']['UF_DEPARTMENT']))
	{
		$arResult['CURRENT_USER']['DEPARTMENT_TOP'] = CIntranetUtils::GetIBlockTopSection($arResult['CURRENT_USER']['UF_DEPARTMENT']);
		if (intval($arResult['DEPARTMENT']) == $arResult['CURRENT_USER']['DEPARTMENT_TOP'])
			$arResult['ONLY_MINE'] = 'Y';
	}
}

foreach ($arResult['USERS'] as $key => $arUser)
{
	if ($arUser['PERSONAL_PHOTO'])
	{
        $arUser['BIG_PHOTO'] = CFile::GetPath($arUser['PERSONAL_PHOTO']);
		$arImage = CIntranetUtils::InitImage($arUser['PERSONAL_PHOTO'], 50);
		$arUser['PERSONAL_PHOTO'] = $arImage['IMG'];
	}

	$arResult['USERS'][$key] = $arUser;
}

$arResult["arUserField"] = array();
$arRes = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", 0, LANGUAGE_ID);
if (!empty($arRes))
{
	foreach ($arRes as $key => $val)
	{
		$arResult["arUserField"][$key] = $val;
	}
}

$arResult['CURRENT'] = array();
foreach($arResult['USERS'] as $user) {;
    if($user['IS_BIRTHDAY'] === true) {
        $arResult['CURRENT'][] = $user;
    }
}
?>
<?$comments = array();?>
<?foreach($arResult["CURRENT"] as &$current):?>
<?if(in_array($current["ID"],$arResult["ELEMENTS_ID"])):?>
        <?
        $rsData = $entity_data_class::getList(array(
        'select' => array('UF_ID', 'UF_COMMENT'),
        'order' => array('ID' => 'DESC'),
        'limit' => '1',
        'filter' => array('UF_ID' => $current["ID"])
        ));
        while($el = $rsData->fetch()){
            $comments[$el["UF_ID"]] = $el["UF_COMMENT"];
        }
        ?>
<?endif;?>
    <?if(strlen($current["NAME"].$current["LAST_NAME"] > 15)):
        $current["NAME"] = $current["NAME"]."<br>".$current["LAST_NAME"];?>
    <?else:
        $current["NAME"] = $current["NAME"]." ".$current["LAST_NAME"];?>
    <?endif;?>
<?endforeach;?>

    <?if(!empty($comments)):
        $arResult["COMMENTS_BY_ID"] = $comments;
    endif;
    ?>
