<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams["NUM_USERS"] = intval($arParams["NUM_USERS"]);
$arParams["NUM_USERS"] = ($arParams["NUM_USERS"] > 0 && $arParams["NUM_USERS"] <=50 ? $arParams["NUM_USERS"] : 5);

$arParams["LIST_URL"] = ($arParams["LIST_URL"]?$arParams["LIST_URL"]:"/company/birthdays.php");

?>

<?$APPLICATION->IncludeComponent(
	"mcart:intranet.structure.birthday.nearest",
	"include_area",
	Array(
		"STRUCTURE_PAGE" => $arParams["STRUCTURE_PAGE"],
		"DETAIL_URL" => $arParams["DETAIL_URL"],
		"PM_URL" => $arParams["PM_URL"],
		"PATH_TO_CONPANY_DEPARTMENT" => $arParams["PATH_TO_CONPANY_DEPARTMENT"],
		"PATH_TO_VIDEO_CALL" => $arParams["PATH_TO_VIDEO_CALL"],
		"STRUCTURE_FILTER" => "structure",
		"NUM_USERS" => $arParams["NUM_USERS"],
		"AJAX_MODE" => "N",
		"AJAX_OPTION_SHADOW" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"DATE_TIME_FORMAT" => $arParams["DATE_TIME_FORMAT"],
		"SHOW_YEAR" => $arParams["SHOW_YEAR"],
		"USER_PROPERTY" => $arParams["USER_PROPERTY"],
		"DEPARTMENT" => $arParams["DEPARTMENT"],
		"NAME_TEMPLATE" => $arParams["NAME_TEMPLATE"],
		"SHOW_LOGIN" => $arParams["SHOW_LOGIN"],
		"DATE_FORMAT" => $arParams["DATE_FORMAT"],
		"DATE_FORMAT_NO_YEAR" => $arParams["DATE_FORMAT_NO_YEAR"],
	),
	false,
	Array("HIDE_ICONS"=>"Y")
);?>
