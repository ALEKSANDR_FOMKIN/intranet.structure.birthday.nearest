<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arDescription = Array(
		"NAME"=>GetMessage("GD_BIRTHDAY_NAME_2"),
		"DESCRIPTION"=>GetMessage("GD_BIRTHDAY_DESC_2"),
		"ICON"=>"gadget.png",
		"GROUP"=> Array("ID"=>"employees"),
		"CAN_BE_FIXED"=> true,
	);
?>
